CARGO_TOML_FILES=$(find . -name "Cargo.toml")
rm rust-toolchain
for CARGO_TOML in $CARGO_TOML_FILES; do
    DIR=$(dirname "$CARGO_TOML")
    cd "$DIR"
    rm -f Cargo.lock
    echo "Building $DIR"
    cd $DIR
    cargo build --release
    cargo test
    cd - > /dev/null
done
