PACKAGE_NAME_START="tezos-smart-rollup"
CARGO_TOML_FILES=$(find . -name "Cargo.toml")
LATEST_VERSION=$(curl -s https://crates.io/api/v1/crates/tezos-smart-rollup | grep -o '"num":"[^"]*' | awk -F '"' '{print $4}' | head -1)
for CARGO_TOML in $CARGO_TOML_FILES; do
    DIR=$(dirname "$CARGO_TOML")
    cd "$DIR"
    PACKAGE_LINES=$(grep -oE "${PACKAGE_NAME_START}[^ =]*" Cargo.toml)
    for package in "${PACKAGE_LINES[@]}"; do
        if [ -n "$package" ]; then
            echo "Found '$package' in '$DIR'."
            PACKAGE_NAME=$(echo "$package" | grep -o "${PACKAGE_NAME_START}\(-[a-zA-Z]\+\)\?")
            echo "Package name: $PACKAGE_NAME"
            # Update the version
            cargo add ${PACKAGE_NAME}@${LATEST_VERSION}
        fi
    done
    cd - > /dev/null
done
# Commit and push the changes
PR_TITLE="Updating ${PACKAGE_NAME_START} [$LATEST_VERSION]"
BRANCH_NAME="base-image-refs-update-${LATEST_VERSION}"
RESPONSE=(curl "https://gitlab.com/api/v4/projects/tezos%2Fkernel-gallery/merge_requests?source_branch=base-image-refs-update-2023-11-12-0505")

if [[ $(echo "$RESPONSE" | jq length) -gt 0 ]]; then
    echo "Merge request with title '$merge_request_title' already exists."
else
    git remote set-url origin \
        "https://gitlab-ci-token:${CI_COMMIT_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git" || true
    git config --global user.email "${CI_COMMIT_AUTHOR_EMAIL}"
    git config --global user.name "${CI_COMMIT_AUTHOR_NAME}"
    git checkout -b "${BRANCH_NAME}"
    git add .
    if git diff --cached --quiet; then
        echo "No changes to commit."
        exit 0
    fi
    git commit -m "Updating ${PACKAGE_NAME_START}"
    git push \
        -o merge_request.create \
        -o merge_request.title="${PR_TITLE}" \
        origin "${BRANCH_NAME}" 2>&1 | tee push.info
fi
